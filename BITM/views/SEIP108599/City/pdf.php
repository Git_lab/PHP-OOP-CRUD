<?php

include_once '../../../vendor/mpdf/mpdf/mpdf.php';
include_once ('../../../vendor/autoload.php');

use App\BITM\SEIP108599\City\city;

$City = new city();
$data = $City->index();

$trs = "";
$serial = 0;
foreach ($data as $value):
    $serial++;
    $trs.="<tr>";
    $trs.="<td align='center'>" . $serial . "</td>";
    $trs.="<td align='center'>" . $value['id'] . "</td>";
    $trs.="<td align='center'>" . $value['name'] . "</td>";
    $trs.="<td align='center'>" . $value['city'] . "</td>";
    $trs.="</tr>";
endforeach;

$html = <<<EOD
   <!DOCTYPE html>
    <html>
        <head>
            <title>Bitm Project</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </head>
        <body>
            <h1>View ALL City data</h1>
            <table  align="center" width="562" border="1" cellpadding="0" cellspacing="0" >
                <thead>
                <tr>
                    <td align="center">SL NO</td>
                    <td align="center">ID</td>
                    <td align="center">Name</td>
                    <td align="center">Email</td>
                </tr>
                </thead>

                <tbody>
                   $trs;
                </tbody>
            </table>
        </body>
EOD;

$mpdf = new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output('file.pdf', 'D'); //()Eta Blanck Rakle Just Open Hobe And Data Dekhabe//
?>