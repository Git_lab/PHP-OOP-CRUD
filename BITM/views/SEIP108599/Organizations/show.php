<?php
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP108599\Organizations\organizations;

$organizations= new organizations();
$data=$organizations->prepare($_GET)->show();
//var_dump($data);
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <script src="../../../js/jquery-1.11.1.min.js"></script>
        <script src="../../../js/list.min.js"></script>


        <link rel="stylesheet" href="../../../css/newtab.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/common.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/general.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/add-edit-form.css" type="text/css" />
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" />

        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" /></head>
    <body>
        <div class="crud-form" data-unique-hash="a2ad475d60b22954085c657a73309b8f">
            <div class="container gc-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-label">
                            <div class="floatL l5">
                                View organizations                    
                            </div>
                            <div class="floatR r5 minimize-maximize-container minimize-maximize">
                                <i class="fa fa-caret-up"></i>
                            </div>
                            <div class="floatR r5 gc-full-width">
                                <i class="fa fa-expand"></i>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="form-container table-container">
                            <form action="/demo/bootstrap_theme/update/103" method="post" id="crudForm"  enctype="multipart/form-data" class="form-horizontal" accept-charset="utf-8">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        ID:
                                    </label>
                                    <div class="col-sm-9 read-row">
                                        <div id="field-customerName" class="readonly_label"><?php echo $data->id; ?></div>                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                         Name:
                                    </label>
                                    <div class="col-sm-9 read-row">
                                        <div id="field-contactLastName" class="readonly_label"><?php echo $data->name; ?></div>                            
                                    </div>
                                </div>
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                         Summary:
                                    </label>
                                    <div class="col-sm-9 read-row">
                                        <div id="field-contactLastName" class="readonly_label"><?php echo $data->summary; ?></div>                            
                                    </div>
                                </div>


                                <div id='report-error' class='report-div error'></div>
                                <div id='report-success' class='report-div success'></div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-7">
                                        <button class="btn btn-default cancel-button" type="button" onclick="window.location = 'index.php'" >
                                            <i class="fa fa-arrow-left"></i>
                                            Back to list                                
                                        </button>
                                    </div>
                                </div>
                            </form>                
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var validation_url = '/demo/bootstrap_theme/update_validation/103';
            var list_url = '/demo/bootstrap_theme';

            var message_alert_edit_form = "The data you had change may not be saved.\nAre you sure you want to go back to list?";
            var message_update_error = "An error has occurred on saving.";
        </script><script type="text/javascript">
            var js_date_format = 'dd/mm/yy';
        </script>
        <script type="text/javascript">
            var default_javascript_path = '/assets/grocery_crud/js';
            var default_css_path = '/assets/grocery_crud/css';
            var default_texteditor_path = '/assets/grocery_crud/texteditor';
            var default_theme_path = '/assets/grocery_crud/themes';
            var base_url = '/';

        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-23493740-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script></body>
</html>