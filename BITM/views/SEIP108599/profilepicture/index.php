<?php
include_once ('../../../vendor/autoload.php');
session_start();
use App\BITM\SEIP108599\profilepicture\imageuplode;
use App\BITM\SEIP108599\profilepicture\Message;

$profile= new imageuplode();
$data=$profile->index();

?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <script src="../../../js/jquery-1.11.1.min.js"></script>
        <script src="../../../js/list.min.js"></script>
        
        <link rel="stylesheet" href="../../../css/newtab.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/common.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/list.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/general.css" type="text/css" />
        <link rel="stylesheet" href="../../../css/animate.min.css" type="text/css" />
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" />

        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </head>
    <body>
        <div class="warning" style="color: green;">
            <?php
               if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message']))
               {
                   echo Message::message();
               }
            ?>
        </div>
        <script type="text/javascript">
            //var dialog_forms = '';
            $(document).ready(function(){
               $(".warning").show().delay(500).fadeOut(15000); 
            });
            

        </script>
        <script type='text/javascript'>
            var base_url = '/';

            var subject = 'Customer';
            var ajax_list_info_url = '/demo/bootstrap_theme/ajax_list_info';
            var ajax_list_url = '/demo/bootstrap_theme/ajax_list';
            var unique_hash = 'a2ad475d60b22954085c657a73309b8f';

            var message_alert_delete = "Are you sure that you want to delete this record?";
            var THEME_VERSION = '1.3.1';

        </script>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                 <a href="../../../index.php" class="btn btn-default t5 gc-print">
                      <i class="fa fa-home floatL t3"></i>
                      <span class="hidden-xs floatL l5">
                          Go To Home                                        
                      </span>
                      <div class="clear"></div>
                  </a>
                </div>
            </div>
       </div>
       <br/>
       <br/>
        
        <div class="container gc-container">
            <div class="success-message hidden"></div>

            <div class="row">
                <div class="table-section">
                    <div class="table-label">
                        <div class="floatL l5">
                            Profie Picture                    
                         </div>                  
                        <div class="floatR r5 minimize-maximize-container minimize-maximize">
                            <i class="fa fa-caret-up"></i>
                        </div>
                        <div class="floatR r5 gc-full-width">
                            <i class="fa fa-expand"></i>                        
                        </div>                      
                        <div class="clear"></div>
                    </div>
                    <div class="table-container">
                        <form action="/demo/bootstrap_theme" method="post" autocomplete="off" id="gcrud-search-form" accept-charset="utf-8">                        <div class="header-tools">
                                <div class="floatL t5">
                                    <a class="btn btn-default" href="create.php"><i class="fa fa-plus"></i> &nbsp; Add Mobile</a>
                                    <a class="btn btn-default" href="trashed.php"><i class="fa fa-trash"></i> 
                                    &nbsp; View All Trash Data</a>
                                </div>
                                <div class="floatR">
                                    
                                    <a href="pdf.php" class="btn btn-default t5 gc-export">
                                        <i class="fa fa-file-pdf-o floatL t3"></i>
                                        <span class="hidden-xs floatL l5">
                                            Downlode PDF                                        
                                        </span>
                                        <div class="clear"></div>
                                    </a>
                                    <a href="xl.php" class="btn btn-default t5 gc-print">
                                        <i class="fa fa-file-excel-o floatL t3"></i>
                                        <span class="hidden-xs floatL l5">
                                            Downlode Excel                                        
                                        </span>
                                        <div class="clear"></div>
                                    </a>
                                    <a href="phpmailer.php" class="btn btn-default t5 gc-print">
                                        <i class="fa fa-envelope floatL t3"></i>
                                        <span class="hidden-xs floatL l5">
                                            Go To Email                                        
                                        </span>
                                        <div class="clear"></div>
                                    </a>

                                    <a class="btn btn-primary search-button t5">
                                        <i class="fa fa-search"></i>
                                        <input type="text" name="search" class="search-input" />
                                    </a>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <table class="table table-bordered grocery-crud-table table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2" > Actions </th>
                                        <th class="column-with-ordering" data-order-by="customerName">SL NO</th>
                                        <th class="column-with-ordering" data-order-by="contactLastName">ID</th>
                                        <th class="column-with-ordering" data-order-by="phone">Name</th>
                                        <th class="column-with-ordering" data-order-by="phone">Profile Picture</th>
                                    </tr>

                                    <tr class="filter-row gc-search-row">
                                        <td class="no-border-right ">
                                            <div class="floatL t5">
                                                <input type="checkbox" class="select-all-none" />
                                            </div>
                                        </td>
                                        <td class="no-border-left ">
                                            <div class="floatL">
                                                <a href="javascript:void(0);" title="Delete"
                                                   class="hidden btn btn-default delete-selected-button">
                                                    <i class="fa fa-trash-o text-danger"></i>
                                                    <span class="text-danger">Delete</span>
                                                </a>
                                            </div>
                                            <div class="floatR l5">
                                                <a href="javascript:void(0);" class="btn btn-default gc-refresh">
                                                    <i class="fa fa-refresh"></i>
                                                </a>
                                            </div>
                                            </div>
                                            <div class="clear"></div>
                                        </td>
                                    </tr>

                                </thead>
                                <tbody>
                                    <?php 
                                        $serial=0;
                                        foreach($data as $value)
                                        {
                                         $serial++;
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="select-row" data-id="103" />
                                        </td>

                                        <td>
                                            <div class="only-desktops"  style="white-space: nowrap">
                                                <a class="btn btn-default" href="edit.php?id=<?php echo $value['id']; ?>"><i class="fa fa-pencil"></i> Edit</a>

                                                <div class="btn-group dropdown">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        More <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="show.php?id=<?php echo $value['id']; ?>"><i class="fa fa-eye"></i> View</a>
                                                        </li>
                                                        <li>
                                                            <a href="trash.php?id=<?php echo $value['id']; ?>"><i class="fa fa-trash"></i> Trash</a>
                                                        </li>
                                                        <li>
                                                            <a href="delete.php?id=<?php echo $value['id']; ?>" title="Delete"  onClick="return confirm('Are you sure to Delete this Page?');" ><!--class="delete-row"-->
                                                                <i class="fa fa-trash-o text-danger"></i>
                                                                <span class="text-danger">Delete</span>
                                                            </a>

                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                            
                                            <div class="only-mobiles">
                                                <div class="btn-group dropdown">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        Actions                            <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="/demo/bootstrap_theme/edit/131">
                                                                <i class="fa fa-pencil"></i> Edit                                </a>
                                                        </li>
                                                        <li>
                                                            <a href="show.php?id=<?php echo $value['id']; ?>"><i class="fa fa-eye"></i> View</a>
                                                        </li>
                                                        <li>
                                                            <a data-target="/demo/bootstrap_theme/delete/131" href="javascript:void(0)" title="Delete" class="delete-row">
                                                                <i class="fa fa-trash-o text-danger"></i> <span class="text-danger">Delete</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                        
                                        <td> <?php echo $serial; ?> </td>
                                        <td> <?php echo $value['id']; ?>  </td>
                                        <td> <?php echo $value['name']; ?> </td>
                                        <td align="center"> 
                                            <img src="uplode/<?php echo $value['image'];?>" width="150" height="150">
                                        </td>
                                       
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <!-- Table Footer -->
                            <div class="footer-tools">

                                <!-- "Show 10/25/50/100 entries" (dropdown per-page) -->
                                <div class="floatL t20 l5">
                                    <!--<div class="floatL t10">
                                        Show                                                 
                                     </div>
                                    <div class="floatL r5 l5 t3">
                                        <select name="per_page" class="per_page form-control">
                                            <option value="10"
                                                    selected="selected">
                                                10&nbsp;&nbsp;
                                            </option>
                                            <option value="25"
                                                    >
                                                25&nbsp;&nbsp;
                                            </option>
                                            <option value="50"
                                                    >
                                                50&nbsp;&nbsp;
                                            </option>
                                            <option value="100"
                                                    >
                                                100&nbsp;&nbsp;
                                            </option>
                                        </select>
                                    </div>
                                    <div class="floatL t10">
                                        entries                                                
                                     </div>
                                    <div class="clear"></div>
                                </div>-->
                                <!-- End of "Show 10/25/50/100 entries" (dropdown per-page) -->


                                <div class="floatR r5">

                                    <!-- Buttons - First,Previous,Next,Last Page -->
                                    <!--<ul class="pagination">
                                        <li class="disabled paging-first"><a href="#"><i class="fa fa-step-backward"></i></a></li>
                                        <li class="prev disabled paging-previous"><a href="#">
                                        <i class="fa fa-chevron-left"></i></a></li>
                                        <li>
                                            <span class="page-number-input-container">
                                                <input type="number" value="1" class="form-control page-number-input" />
                                            </span>
                                        </li>
                                        <li class="next paging-next"><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                                        <li class="paging-last"><a href="#"><i class="fa fa-step-forward"></i></a></li>
                                    </ul>-->
                                    <!-- End of Buttons - First,Previous,Next,Last Page -->

                                    <!--<input type="hidden" name="page_number" class="page-number-hidden" value="1" />

                                    <!-- Start of: Settings button -->
                                  <!--  <div class="btn-group floatR t20 l10 settings-button-container">
                                        <button type="button" class="btn btn-default dropdown-toggle settings-button" 
                                        data-toggle="dropdown">
                                            <i class="fa fa-cog r5"></i>
                                            <span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="javascript:void(0)" class="clear-filtering">
                                                    <i class="fa fa-eraser"></i> Clear filtering
                                                </a>
                                            </li>
                                        </ul>
                                    </div>-->
                                    <!-- End of: Settings button -->

                                </div>


                                <!-- "Displaying 1 to 10 of 116 items" -->
                                <!--<div class="floatR r10 t30">
                                    Displaying <span class="paging-starts">1</span> to <span class="paging-ends">10</span> 
                                    of <span class="current-total-results">122</span> items                                                <span class="full-total-container hidden">
                                                   (filtered from <span class='full-total'>122</span> total entries)                                                </span>
                                </div>
                                <!-- End of "Displaying 1 to 10 of 116 items" -->

                                <!--<div class="clear"></div>
                            </div>-->
                            <!-- End of: Table Footer -->

                        </form>                </div>
                </div>

                <!-- Delete confirmation dialog -->
                <div class="delete-confirmation modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Delete</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure that you want to delete this record?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a href="delete.php?id=<?php echo $value['id']; ?>" class="btn btn-danger" role="button">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Delete confirmation dialog -->

                <!-- Delete Multiple confirmation dialog -->
                <div class="delete-multiple-confirmation modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Delete</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure that you want to delete this record?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Cancel                            </button>
                                <button type="button" class="btn btn-danger delete-multiple-confirmation-button"
                                        data-target="/demo/bootstrap_theme/delete_multiple">
                                    Delete                            </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Delete Multiple confirmation dialog -->

            </div>
        </div><script type="text/javascript">
            var default_javascript_path = '/assets/grocery_crud/js';
            var default_css_path = '/assets/grocery_crud/css';
            var default_texteditor_path = '/assets/grocery_crud/texteditor';
            var default_theme_path = '/assets/grocery_crud/themes';
            var base_url = '/';

        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-23493740-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script></body>
</html>