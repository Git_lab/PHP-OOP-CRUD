<?php

include_once ('../../../vendor/autoload.php');

use App\BITM\SEIP108599\profilepicture\imageuplode;

$profile= new imageuplode();
$data=$profile->prepare($_POST)->edit();

if(isset($_FILES['image'])&&(!empty($_FILES['image']['name']))){
      $errors= array();
      $file_name = time().$_FILES['image']['name'];
      $file_size =$_FILES['image']['size'];
      $file_tmp =$_FILES['image']['tmp_name'];
      $file_type=$_FILES['image']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
      
      $expensions= array("jpeg","jpg","png");
      
      if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
      
      if($file_size > 2097152){
         $errors[]='File size must be excately 2 MB';
      }
      
      if(empty($errors)==true){
          $Path = $_SERVER['DOCUMENT_ROOT'].'/BITM/Project/BITM/views/SEIP108599/profilepicture/uplode/'.$data['image'];
          chown($Path, 666);
         unlink($_SERVER['DOCUMENT_ROOT'].'/BITM/Project/BITM/views/SEIP108599/profilepicture/uplode/'.$data['image']);
         move_uploaded_file($file_tmp,"uplode/".$file_name);
         //$_POST['image']=$file_name;
         //echo "Success";
      }else{
         print_r($errors);
      }
   }
if(isset($_FILES['image'])&&(!empty($_FILES['image']['name']))){ 
$_POST['image']= $file_name;
}

$profile->prepare($_POST)->update();
?>
