<?php
error_reporting(E_ALL);
error_reporting (E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once '../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once ('../../../vendor/autoload.php');

use App\BITM\SEIP108599\Gender\gender;

$gender= new Gender();
$data=$gender->index();

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'SL')
            ->setCellValue('B1', 'ID')
            ->setCellValue('C1', 'User Name')
            ->setCellValue('D1', 'Gender');

$counter=2;
$serial=0;

foreach ($data as $value):
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$counter, $serial)
            ->setCellValue('B'.$counter, $value['id'])
            ->setCellValue('C'.$counter, $value['user_name'])
            ->setCellValue('D'.$counter, $value['gender']);
    $counter++;
endforeach;

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Mobile_list');



// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="File.xls"'); //Ekhane .xls Diya Config korte hoy//
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;