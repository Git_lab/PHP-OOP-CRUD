<?php
namespace App\BITM\SEIP108599\Birthday;
use App\BITM\SEIP108599\Birthday\Message;
use App\BITM\SEIP108599\Birthday\Utility;
use PDO;

class Birthday{
    public $id="";
    public $name="";
    public $date="";
    public $deleted_at="";
    public $con;
    public $user='root';
    public $pass='';
    
       public function __construct() {
        $this->con = new PDO('mysql:host=localhost;dbname=bitm_project', $this->user, $this->pass);
    }
    public function prepare($data = array()) {
        if (is_array($data) && array_key_exists('name', $data) && array_key_exists('date', $data)) {
            $this->name = $data['name'];
            $this->date = $data['date'];
        }
        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store() {
        if(!empty($this->name) && !empty($this->date))
        {
            $query = "INSERT INTO birthday (`id`, `name`, `date`) VALUES (NULL, :name, :date)";
            $result = $this->con->prepare($query);
            $result->execute(array(':name'=>$this->name,':date'=>$this->date));
            if($result)
            {
               Message::message('Birthday Data is Added SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            } 
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        }
           
            //echo($result == true ? 'Data added successfully' : 'Data added Unsuccessfully');
    }
    public function index()
    {
        $_allbirthday=array();
        $query="SELECT * FROM birthday WHERE deleted_at IS NULL";
        $row=  $this->con->query($query);
         $_allbirthday=$row->fetchAll(PDO::FETCH_ASSOC);
        return $_allbirthday;    
    }
    public function show()
    {
        $query="SELECT * FROM birthday WHERE id=:id";
         $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        $result = $row->fetch(PDO::FETCH_OBJ);
        return $result;
    }
     public function edit()
    {
        $query="SELECT * FROM birthday WHERE id=:id";
        $row=  $this->con->prepare( $query);
         $row->execute(array(':id'=>$this->id));
         $result = $row->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function update()
    {
        if(!empty($this->name) && !empty($this->date))
        {
            $query="UPDATE birthday SET name=:name,date =:date WHERE id =:id";
            $row= $this->con->prepare($query);
            $row->execute(array(':id'=>$this->id,':name'=>$this->name,':date'=>$this->date));
            if($row)
            {
               Message::message(' Data is Updated SuccessFully.'); 
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            }    
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        }
    }
    public function delete()
    {
        $query="DELETE FROM birthday WHERE id=:id";
        $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
         if($row)
        {
           Message::message('Mobile Data is Deleted SuccessFully.');
           Utility::redirect();
        }
        else 
        {
            Message::message('There is an Error While Storing Mobile Information,Please Try Again');
            Utility::redirect();
        }
    }
      public function trash() {
        $this->deleted_at=date("Y-m-d"); 
        $query="UPDATE birthday SET deleted_at=:deleted_at WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id,':deleted_at'=>$this->deleted_at));
        if($row)
         {
            Message::message('Birthday Information has Been Trashed SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Trash'); 
             Utility::redirect();
         }
      }
      public function trashed() {
        $query = "SELECT * FROM birthday WHERE `deleted_at` IS NOT NULL" ;
        $result = $this->con->query( $query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;        
    }
     public function recover() {
        $query="UPDATE birthday SET deleted_at=NULL WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
         if($row)
         {
            Message::message('Birthday Information has Been Recovered SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Recover');
             Utility::redirect();
         }
    }
}
