<?php
namespace App\BITM\SEIP108599\Email;
use App\BITM\SEIP108599\Email\Message;
use App\BITM\SEIP108599\Email\Utility;
use PDO;

class email{
    public $id="";
    public $name="";
    public $email="";
    public $deleted_at="";
    public $con;
    public $username='root';
    public $password='';
    
    public function __construct()
    {
        try 
        {
            $this->con= new PDO('mysql:host=localhost;dbname=bitm_project',$this->username,$this->password);
            $this->con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            
        }catch (PDOException $ex) {
            echo 'ERROR'; 
        }
        
        //$con=  mysql_connect('localhost','root','') or die("Cannot Connect");
       // mysql_select_db('bitm_project');
    }
    public function prepare($data = array()) {
        if (is_array($data) && array_key_exists('name', $data) && array_key_exists('email', $data)) {
            $this->name = $data['name'];
            $this->email = $data['email'];
        }
        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store() {
        if(!empty($this->name) && !empty($this->email))
        {
            $query = "INSERT INTO email (`id`, `name`, `email`) VALUES (NULL, :name, :email)";
            $result = $this->con->prepare($query);
            $result->execute(array(':name'=>$this->name,':email'=>$this->email ));
            if($result)
            {
               Message::message('Mobile Data is Added SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            }  
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        }
            //echo($result == true ? 'Data added successfully' : 'Data added Unsuccessfully');
    }
    public function index()
    {
        $_allemail=array();
        $query="SELECT * FROM email WHERE deleted_at IS Null "; ///WHERE deleted_at IS Null
        $row=  $this->con->query($query);
         $_allemail=$row->fetchAll(PDO::FETCH_ASSOC);
        return $_allemail;    
    }
    public function show()
    {
        $query="SELECT * FROM email WHERE id=:id";
         $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        $result = $row->fetch(PDO::FETCH_OBJ);
        return $result;
    }
     public function edit()
    {
        $query="SELECT * FROM email WHERE id=:id";
        $row=  $this->con->prepare( $query);
         $row->execute(array(':id'=>$this->id));
         $result = $row->fetch(PDO::FETCH_OBJ);
        return $result;
    }
    public function update()
    {
        if(!empty($this->name) && !empty($this->email))
        {
            $query="UPDATE email SET name=:name,email =:email WHERE id =:id";
            $row= $this->con->prepare($query);
            $row->execute(array(':id'=>$this->id,':name'=>$this->name,':email'=>$this->email));
            if($row)
            {
               Message::message('Email Data is Updated SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            } 
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        }
         return true;
    }
    public function delete()
    {
        $query="DELETE FROM email WHERE id=:id";
        $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        if($row)
        {
           Message::message('Email Data is Deleted SuccessFully.');
           Utility::redirect();
        }
        else 
        {
            Message::message('There is an Error While Storing Mobile Information,Please Try Again');
            Utility::redirect();
        }
    }
      public function trash() {
        $this->deleted_at=date("Y-m-d"); 
        $query="UPDATE email SET deleted_at=:deleted_at WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id,':deleted_at'=>$this->deleted_at));
        if($row)
         {
            Message::message('Email Information has Been Trashed SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Trash'); 
             Utility::redirect();
         }
      }
      public function trashed() {
        $query = "SELECT * FROM email WHERE `deleted_at` IS NOT NULL";
        $result = $this->con->query($query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }
     public function recover() {
        $query="UPDATE email SET deleted_at=NULL WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        if($row)
         {
            Message::message('Email Information has Been Recovered SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Recover');
             Utility::redirect();
         }
    }
}
