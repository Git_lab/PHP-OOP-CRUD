<?php
namespace App\BITM\SEIP108599\Hobby;
use App\BITM\SEIP108599\Hobby\Message;
use App\BITM\SEIP108599\Hobby\Utility;

class Hobbies
{
    public $id='';
    public $user_name='';
    public $hobby='';
    
    public function __construct()
    {
        
        $con=  mysql_connect('localhost','root','') or die("Cannot Connect");
        mysql_select_db('bitm_project');
    }
    public function prepare($data=array()){
       
        if(is_array($data) && array_key_exists('user_name',$data) && array_key_exists('hobby',$data))
        {
          
            $this->user_name=$data['user_name'];
            $this->hobby=$data['hobby'];
        }
        if (array_key_exists('id',$data) && !empty($data['id']))
        {
            $this->id=$data['id']; 
        }
        return $this;   
    }
    public function store()
    {
        if(!empty($this->user_name) && !empty($this->hobby))
        {
            $query="INSERT INTO hobbies (`id`,`user_name`,`hobby`) values (NULL,'".$this->user_name."','".$this->hobby."')";
            $row=  mysql_query($query);
            if($row)
            {
               Message::message('Data has Been Inserted Successfully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            }
        }
        else 
        {
            Message::message('Faild Empty.');
             Utility::redirect();
        }
    }
    public function index()
    {
        $_allHobbies=array();
        $query="SELECT * FROM hobbies WHERE delete_add IS NULL";
        $row=  mysql_query($query);
        while ($result=mysql_fetch_assoc($row))
        {
            $_allHobbies[]=$result;
        }
        return $_allHobbies;
    }
    public function show()
    {
        $query="SELECT * FROM hobbies WHERE id='".$this->id."'";
        $row=  mysql_query($query);
        $result=  mysql_fetch_assoc($row);
        return $result;
    }
    public function edit()
    {
        $query="SELECT * FROM hobbies WHERE id='".$this->id."'";
        $row=  mysql_query($query);
        $result=  mysql_fetch_assoc($row);
        return $result;
    }
    public function update()
    {
        if(!empty($this->user_name) && !empty($this->hobby))
        {
            $query="UPDATE hobbies SET user_name='".$this->user_name."',hobby = '".$this->hobby."' WHERE id = '".$this->id."'";
            $row=mysql_query($query);
            if($row)
            {
               Message::message('Data Update Successfully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            }
        }
        else 
        {
            Message::message('Faild Empty.');
             Utility::redirect();
        }
        
    }
    public function delete()
    {
        $query="DELETE FROM hobbies WHERE id='".$this->id."'";
        $row= mysql_query($query);
        if($row)
        {
           Message::message('Data Delete Successfully.');
           Utility::redirect();
        }
        else 
        {
            Message::message('There is an Error While Storing Mobile Information,Please Try Again');
            Utility::redirect();
        }
    }
    public function trash()
    {
        $this->delete_add= date('Y-m-d');
       $query="UPDATE hobbies SET delete_add='$this->delete_add' WHERE id='".$this->id."'";       
       $result=mysql_query($query);
        if($result)
         {
            Message::message('Mobile Information has Been Trashed SuccessFully.'); 
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Trash');
             Utility::redirect();
         }
       //echo($result==true?'Data Trash successfully':'Data Trash Unsuccessfully'); 
        
    }
    public function trashed()
    {
        $alldata_hobbey=array();
        $query="SELECT * From hobbies WHERE delete_add IS NOT NULL";
        $result=mysql_query($query);
        while($row= mysql_fetch_assoc($result))
        {
             $alldata_hobbey[] = $row;
        }
        return $alldata_hobbey;
    }
    public function recover()
    {
       $query="UPDATE hobbies SET delete_add=NULL WHERE id='".$this->id."'";       
       $result=mysql_query($query);
        if($result)
         {
            Message::message('Mobile Information has Been Recovered SuccessFully.'); 
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Recover');
             Utility::redirect();
         }
        //echo($result==true?'Data Recover successfully':'Data Recover Unsuccessfully'); 
    }
}