<?php
namespace App\BITM\SEIP108599\City;
use App\BITM\SEIP108599\City\Message;
use App\BITM\SEIP108599\City\Utility;
use PDO;

class city{
    public $id="";
    public $name="";
    public $city="";
    public $deleted_at="";
    public $con;
    public $username='root';
    public $password='';
    
    public function __construct()
    {
        try 
        {
            $this->con= new PDO('mysql:host=localhost;dbname=bitm_project',$this->username,$this->password);
            $this->con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            
        }catch (PDOException $ex) {
            echo 'ERROR'; 
        }
        
        //$con=  mysql_connect('localhost','root','') or die("Cannot Connect");
       // mysql_select_db('bitm_project');
    }
    public function prepare($data = array()) {
        if (is_array($data) && array_key_exists('name', $data) && array_key_exists('city', $data)) {
            $this->name = $data['name'];
            $this->city = $data['city'];
        }
        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store() {
        if(!empty($this->name) && !empty($this->city))
        {
            $query = "INSERT INTO city (`id`, `name`, `city`) VALUES (NULL, :name, :city)";
            $result = $this->con->prepare($query);
            $result->execute(array(':name'=>$this->name,':city'=>$this->city ));
            if($result)
            {
               Message::message('City Data is Added SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            }  
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        }
            //echo($result == true ? 'Data added successfully' : 'Data added Unsuccessfully');
    }
    public function index()
    {
        $_allcity=array();
        $query="SELECT * FROM city WHERE deleted_at IS Null"; ///WHERE deleted_at IS Null
        $row=  $this->con->query($query);
         $_allcity=$row->fetchAll(PDO::FETCH_ASSOC);
        return $_allcity;    
    }
    public function show()
    {
        $query="SELECT * FROM city WHERE id=:id";
         $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        $result = $row->fetch(PDO::FETCH_OBJ);
        return $result;
    }
     public function edit()
    {
        $query="SELECT * FROM city WHERE id=:id";
        $row=  $this->con->prepare( $query);
         $row->execute(array(':id'=>$this->id));
         $result = $row->fetch(PDO::FETCH_OBJ);
        return $result;
    }
    public function update()
    {
        if(!empty($this->name) && !empty($this->city))
        {
            $query="UPDATE city SET name=:name,city=:city WHERE id =:id";
            $row= $this->con->prepare($query);
            $row->execute(array(':id'=>$this->id,':name'=>$this->name,':city'=>$this->city));
            if($row)
            {
               Message::message('City Data is Updated SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            } 
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        }
         return true;
    }
    public function delete()
    {
        $query="DELETE FROM city WHERE id=:id";
        $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        if($row)
        {
           Message::message('Email Data is Deleted SuccessFully.');
           Utility::redirect();
        }
        else 
        {
            Message::message('There is an Error While Storing Mobile Information,Please Try Again');
            Utility::redirect();
        }
    }
      public function trash() {
        $this->deleted_at=date("Y-m-d"); 
        $query="UPDATE city SET deleted_at=:deleted_at WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id,':deleted_at'=>$this->deleted_at));
        if($row)
         {
            Message::message('City Information has Been Trashed SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Trash'); 
             Utility::redirect();
         }
      }
      public function trashed() {
        $query = "SELECT * FROM city WHERE `deleted_at` IS NOT NULL";
        $result = $this->con->query($query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }
     public function recover() {
        $query="UPDATE city SET deleted_at=NULL WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        if($row)
         {
            Message::message('City Information has Been Recovered SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Recover');
             Utility::redirect();
         }
    }
}
