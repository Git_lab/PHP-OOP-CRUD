<?php
namespace App\BITM\SEIP108599\profilepicture;
use App\BITM\SEIP108599\profilepicture\Message;;
use App\BITM\SEIP108599\profilepicture\Utility;
use PDO;

class imageuplode
{
    public $id="";
    public $name="";
    public $image="";
    public $deleted_at="";
    public $con="";
    public $username="root";
    public $password="";


    public function __construct()
    {
        try 
        {
            $this->con= new PDO('mysql:host=localhost;dbname=bitm_project',$this->username,$this->password);
            $this->con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            
        }catch (PDOException $ex) {
            echo 'ERROR'; 
        }
        
        //$con=  mysql_connect('localhost','root','') or die("Cannot Connect");
       // mysql_select_db('bitm_project');
    }
    public function prepare($data=array()){
       
        if(is_array($data) && array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if(is_array($data) && array_key_exists('image',$data))
        {
            $this->image=$data['image'];
        }
        if (array_key_exists('id',$data) && !empty($data['id']))
        {
            $this->id=$data['id']; 
        }
        return $this;   
    }
    public function store()
    {
        if(!empty($this->name) && !empty($this->image))
        {
            $query="INSERT INTO profilepicture (`name`,`image`) values (:name,:image)";
            $row=$this->con->prepare($query);
            $row->execute(array(':name'=>$this->name,':image'=>$this->image));
            if($row)
            {
               Message::message('Profile Data is Added SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            }
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        }
       
    }
    public function index()
    {
        $_allimage=array();
        $query="SELECT * From profilepicture WHERE deleted_at IS NULL";
        $row=  $this->con->query($query) or die("Faield");
        while ($result=$row->fetch(PDO::FETCH_ASSOC))
        {
            $_allimage[]=$result;
        }
        return $_allimage;
    }
    public function show()
    {
        $query="SELECT * FROM profilepicture WHERE id=:id";
        $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        $result=$row->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function delete()
    {
        $query="DELETE FROM profilepicture WHERE id=:id";
        $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        if($row)
        {
           Message::message(' Data is Deleted SuccessFully.');
           Utility::redirect();
        }
        else 
        {
            Message::message('There is an Error While Storing Mobile Information,Please Try Again');
            Utility::redirect();
        }
    }
    public function edit()
    {
        $query="SELECT * FROM profilepicture WHERE id=:id";
        $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        $result=$row->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function update()
    {
        if(!empty($this->image))
        {
            $query="UPDATE profilepicture SET name=:name,image=:image WHERE id=:id";
            $row=  $this->con->prepare($query);
            $row->execute(array(':name'=>$this->name,':image'=>$this->image,':id'=>$this->id));
        }
        else 
        {
            $query="UPDATE profilepicture SET name=:name WHERE id=:id";
            $row=  $this->con->prepare($query);
            $row->execute(array(':name'=>$this->name,':id'=>$this->id));
        }
            if($row)
            {
                Message::message('Data Update Successfully.');
                Utility::redirect();
            }
            else 
            {
                Message::message('Data Update Not Successfully.');
                Utility::redirect();
            }
             return true;
    }
    public function trash() {
        $this->deleted_at=date("Y-m-d"); 
        $query="UPDATE profilepicture SET deleted_at=:deleted_at WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id,':deleted_at'=>$this->deleted_at));
         if($row)
         {
            Message::message('Profile Information has Been Trashed SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Trash'); 
             Utility::redirect();
         }
      }
      public function trashed() {
        $query = "SELECT * FROM profilepicture WHERE `deleted_at` IS NOT NULL" ;
        $result = $this->con->query( $query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;        
    }
     public function recover() {
        $query="UPDATE profilepicture SET deleted_at=NULL WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
         if($row)
         {
            Message::message('Mobile Information has Been Recovered SuccessFully.'); 
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Recover'); 
             Utility::redirect();
         }
    }
}
   
?>