<?php
namespace App\BITM\SEIP108599\profilepicture;
if(!isset($_SESSION))
{
    session_start();
}
class Message
{
    static public function message($message=NULL)
    {
        if (is_null($message)) //Please Give Me Message
        {
            $_message=  self::getMessage();
            return $_message;
        }
        else //Please Set This Message
        {
            self::setMessage($message);
        }
    }
    
    static private function getMessage()
    {
        $_message=$_SESSION['message'];
        $_SESSION['message']="";
        return $_message;
    }
    static private function setMessage($message)
    {
        $_SESSION['message']=$message;
    }
}
