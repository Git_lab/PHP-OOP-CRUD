<?php
namespace App\BITM\SEIP108599\Organizations;
use App\BITM\SEIP108599\Organizations\Message;
use App\BITM\SEIP108599\Organizations\Utility;
use PDO;

class organizations{
    public $id="";
    public $name="";
    public $summary="";
    public $deleted_at="";
    public $con;
    public $user='root';
    public $pass='';
    
       public function __construct() {
        $this->con = new PDO('mysql:host=localhost;dbname=bitm_project', $this->user, $this->pass);
    }
    public function prepare($data = array()) {
        if (is_array($data) && array_key_exists('name', $data) && array_key_exists('summary', $data)) {
            $this->name = $data['name'];
            $this->summary = $data['summary'];
        }
        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store() {
        if(!empty($this->name) && !empty($this->summary))
        {
            $query = "INSERT INTO oganizations (`id`, `name`, `summary`) VALUES (NULL, :name, :summary);";
            $result = $this->con->prepare($query);
            $result->execute(array(':name'=>$this->name,':summary'=>$this->summary ));
            if($result)
            {
               Message::message('Profile Data is Added SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            }  
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        }   
            //echo($result == true ? 'Data added successfully' : 'Data added Unsuccessfully');
    }
    public function index()
    {
        $_alloganizations=array();
        $query="SELECT * FROM oganizations WHERE deleted_at IS Null";
        $row=  $this->con->query($query);
         $_alloganizations=$row->fetchAll(PDO::FETCH_ASSOC);
        return $_alloganizations;    
    }
    public function show()
    {
        $query="SELECT * FROM oganizations WHERE id=:id";
         $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        $result = $row->fetch(PDO::FETCH_OBJ);
        return $result;
    }
     public function edit()
    {
        $query="SELECT * FROM oganizations WHERE id=:id";
        $row=  $this->con->prepare( $query);
         $row->execute(array(':id'=>$this->id));
         $result = $row->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function update()
    {
        if(!empty($this->name) && !empty($this->summary))
        {
            $query="UPDATE oganizations SET name=:name,summary =:summary WHERE id =:id";
            $row= $this->con->prepare($query);
            $row->execute(array(':id'=>$this->id,':name'=>$this->name,':summary'=>$this->summary));
            if($row)
            {
               Message::message(' Data is Updated SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            } 
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        } 
        return true;
    }
    public function delete()
    {
        $query="DELETE FROM oganizations WHERE id=:id";
        $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        if($row)
        {
           Message::message(' Data is Deleted SuccessFully.');
           Utility::redirect();
        }
        else 
        {
            Message::message('There is an Error While Storing Mobile Information,Please Try Again');
            Utility::redirect();
        }
    }
      public function trash() {
        $this->deleted_at=date("Y-m-d"); 
        $query="UPDATE oganizations SET deleted_at=:deleted_at WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id,':deleted_at'=>$this->deleted_at));
        if($row)
         {
            Message::message('Organization Information has Been Trashed SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Trash');
             Utility::redirect();
         }
      }
      public function trashed() {
        $query = "SELECT * FROM oganizations WHERE `deleted_at` IS NOT NULL" ;
        $result = $this->con->query( $query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;        
    }
     public function recover() {
        $query="UPDATE oganizations SET deleted_at=NULL WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        if($row)
         {
            Message::message('Organization Information has Been Recovered SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Recover');
             Utility::redirect();
         }
    }
}
