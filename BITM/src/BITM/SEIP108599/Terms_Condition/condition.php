<?php
namespace App\BITM\SEIP108599\Terms_Condition;
use App\BITM\SEIP108599\Terms_Condition\Message;
use App\BITM\SEIP108599\Terms_Condition\Utility;
use PDO;

class condition{
    public $id="";
    public $name="";
    public $description="";
    public $agre="";
    public $deleted_at="";
    public $con;
    public $user='root';
    public $pass='';
    
       public function __construct() {
        $this->con = new PDO('mysql:host=localhost;dbname=bitm_project', $this->user, $this->pass);
    }
    public function prepare($data = array()) {
        if (is_array($data) && array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if(is_array($data) && array_key_exists('description', $data))
        {
            $this->description = $data['description'];
        }
        if(is_array($data) && array_key_exists('agre', $data))
        {
            $this->agre = $data['agre'];
        }
        
        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store() {
        
        if (!empty($this->name) && !empty($this->description) && !empty($this->agre))
        {
            $query = "INSERT INTO terms_condition (`id`,`name`,`description`,`agre`) values (NULL,:name,:description,:agre)";
            $result = $this->con->prepare($query);
            $result->execute(array(':name'=>$this->name,':description'=>$this->description,':agre'=>$this->agre));
            if($result)
            {
               Message::message('Data is Added SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            }  
        }
        elseif(!empty($this->name) && !empty($this->description) && empty($this->agre)) 
        {
            $query = "INSERT INTO terms_condition (`id`,`name`,`description`,`agre`) values (NULL,:name,:description,:agre)";
            $result = $this->con->prepare($query);
            $result->execute(array(':name'=>$this->name,':description'=>$this->description,':agre'=>$this->agre));
            if($result)
            {
               Message::message('Data is Added SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            } 
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        }
            
        
            //echo($result == true ? 'Data added successfully' : 'Data added Unsuccessfully');
    }
    public function index()
    {
        $_allterms_condition=array();
        $query="SELECT * FROM terms_condition WHERE deleted_at IS Null";
        $row=  $this->con->query($query);
         $_allterms_condition=$row->fetchAll(PDO::FETCH_ASSOC);
        return $_allterms_condition;    
    }
    public function show()
    {
        $query="SELECT * FROM terms_condition WHERE id=:id";
         $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        $result = $row->fetch(PDO::FETCH_OBJ);
        return $result;
    }
     public function edit()
    {
        $query="SELECT * FROM terms_condition WHERE id=:id";
        $row=  $this->con->prepare( $query);
         $row->execute(array(':id'=>$this->id));
         $result = $row->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function update()
    {
        if(!empty($this->name) && !empty($this->description))
        {
            $query="UPDATE terms_condition SET name=:name,description =:description WHERE id =:id";
            $row= $this->con->prepare($query);
            $row->execute(array(':id'=>$this->id,':name'=>$this->name,':description'=>$this->description));
            if($row)
            {
               Message::message(' Data is Updated SuccessFully.');
               Utility::redirect();
            }
            else 
            {
                Message::message('There is an Error While Storing Mobile Information,Please Try Again');
                Utility::redirect();
            } 
        }
        else 
        {
            Message::message('Faild Empty.');
            Utility::redirect();
        } 
        return true;
    }
    public function delete()
    {
        $query="DELETE FROM terms_condition WHERE id=:id";
        $row=  $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        if($row)
        {
           Message::message(' Data is Deleted SuccessFully.');
           Utility::redirect();
        }
        else 
        {
            Message::message('There is an Error While Storing Mobile Information,Please Try Again');
            Utility::redirect();
        }
    }
      public function trash() {
        $this->deleted_at=date("Y-m-d"); 
        $query="UPDATE terms_condition SET deleted_at=:deleted_at WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id,':deleted_at'=>$this->deleted_at));
        if($row)
         {
            Message::message('Terms and Condition Information has Been Trashed SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Trash');
             Utility::redirect();
         }
      }
      public function trashed() {
        $query = "SELECT * FROM terms_condition WHERE `deleted_at` IS NOT NULL" ;
        $result = $this->con->query( $query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;        
    }
     public function recover() {
        $query="UPDATE terms_condition SET deleted_at=NULL WHERE id =:id";
        $row= $this->con->prepare($query);
        $row->execute(array(':id'=>$this->id));
        if($row)
         {
            Message::message('Terms & condition Information has Been Recovered SuccessFully.');
            Utility::redirect();
         }
         else 
         {
             Message::message('Cannot Recover');
             Utility::redirect();
         }
    }
}
