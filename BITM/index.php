<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="js/bootstrap.js"></script>

</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">BITM</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
<br />
<div class="jumbotron">
  <div class="container text-center">
      <img src="image/SBDS 02390.jpg" class="img-thumbnail" alt="Cinque Terre" width="304" height="236"> 
     <h1>Atomic Project</h1>      
    <p>Name:MD Shahiduzzaman Sakib</p>
    <p>ID:108599</p>
    <p>Batch:Web App Developtent(PHP)-Batch-16</p>
  </div>
</div>
  
<div class="container-fluid bg-3 text-center">    
  <h3>Some of My Work...</h3><br>
  <div class="row">
    <div class="col-sm-3">
      <p><a href="views/SEIP108599/Mobile/index.php">Mobile</a></p>
      <a href="views/SEIP108599/Mobile/index.php">
        <img class="image-li" src="image/Mobile .jpg" style="width:100%" alt="Image">'
      </a>
    </div>
    <div class="col-sm-3"> 
      <p><a href="views/SEIP108599/Birthday/index.php">Birthday</a></p>
      <a href="views/SEIP108599/Birthday/index.php">
        <img class="image-li" src="image/Birthday.jpg" class="img-responsive" style="width:100%" alt="Image">
      </a>
    </div>
    <div class="col-sm-3"> 
      <p><a href="views/SEIP108599/Organizations/index.php">Summary of Organizations</a></p>
      <a href="views/SEIP108599/Organizations/index.php">
        <img class="image-li" src="image/summary.jpg" class="img-responsive" style="width:100%" alt="Image">
      </a>
    </div>
    <div class="col-sm-3">
      <p><a href="views/SEIP108599/Email/index.php">Subscription</a></p>
      <a href="views/SEIP108599/Email/index.php">
        <img class="image-li" src="image/Subsciption.jpg" style="width:100%" alt="Image">
      </a>
    </div>
  </div>
</div><br>

<div class="container-fluid bg-3 text-center">    
  <div class="row">
    <div class="col-sm-3">
      <p><a href="views/SEIP108599/profilepicture/index.php">Profile Picture</a></p>
      <a href="views/SEIP108599/profilepicture/index.php">
       <img class="image-li" src="image/Profile.png" class="img-responsive" style="width:100%" alt="Image">
      </a>
    </div>
    <div class="col-sm-3"> 
      <p><a href="views/SEIP108599/Gender/index.php">Gender</a></p>
      <a href="views/SEIP108599/Gender/index.php">
        <img class="image-li" src="image/gender.png" class="img-responsive" style="width:100%" alt="Image">'
      </a>
    </div>
    <div class="col-sm-3"> 
        <p><a href="views/SEIP108599/Terms_Condition/index.php">Terms & Condition</a></p>
      <a href="views/SEIP108599/Terms_Condition/index.php">
      <img class="image-li" src="image/terms-and-conditions.jpg" class="img-responsive" style="width:100%" alt="Image">
      </a>
    </div>
    <div class="col-sm-3">
      <p><a href="views/SEIP108599/Hobby/index.php">Hobby</a></p>
      <a href="views/SEIP108599/Hobby/index.php">
        <img class="image-li" src="image/Hobbey.png" class="img-responsive" style="width:100%" alt="Image">
      </a>
    </div>
  </div>
</div><br>
<div class="container-fluid bg-3 text-center">    
  <div class="row">
    <div class="col-sm-5"></div>
    <div class="col-sm-3">
      <p><a href="">City</a></p>
      <a href="views/SEIP108599/City/index.php"><img class="image-li" src="image/City.jpg" class="img-responsive" style="width:100%" alt="Image"></a>
    </div>
    <div class="col-sm-4"></div>
  </div>
</div>
<br><br>

<footer class="container-fluid text-center">
  <p>Copyright © 2016 .All Rights Reserved Powered by Sakib</p>
</footer>

</body>
</html>
